# prueba_cidenet

prueba de angular / node de cidenet

1. Hacer git clone del proyecto
   git clone  https://gitlab.com/ckomalram/prueba_cidenet.git

2. Levantar humant talent:
    -Ir a la carpeta humantalent
    -Ejecutar npm install
    -Ejecutar docker-compose up -d
    -Ir a la ruta http://localhost:5050/
        -Si no ha levantado, esperar a que levante la interfaz de pg admin.
        -Una vez lenvantada, ingresar con las credenciales de prueba en el archivo de docker-compose
    - Una vez las imagenes de docker esten arriba, Ejecutar migraciones:
        -npm run migration:run
    -Levantar aplicación:
        npm run start:dev
    -Validar http://localhost:3000/docs para ver la información del API creado. 

:::Talent human fue creado con nest js.

3. Levantar el front creado en angular:
    -Irse a la carpeta frontht
    -Ejecutar npm install
    -Ejecutar ng serve
:: Los avances de  frontht  fueron hechos con Angular cli 12.
