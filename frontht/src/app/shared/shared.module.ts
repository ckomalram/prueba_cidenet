import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [FooterComponent],
  exports: [FooterComponent],

  imports: [CommonModule, RouterModule, MaterialModule],
})
export class SharedModule {}
