import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Employee } from '../employees.model';

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  constructor(private http: HttpClient) {}

  getAllEmployee() {
    console.log('Adquiriendo info');
    return this.http.get<Employee[]>(`${environment.url_api}/employee`);
  }

  getEmployee(id: number) {
    return this.http.get<Employee>(`${environment.url_api}/employee/${id}`);
  }

  createEmployee(employee: Employee) {
    return this.http.post<Employee>(
      `${environment.url_api}/employee`,
      employee
    );
  }

  updateEmployee(id: number, changes: Partial<Employee>) {
    return this.http.put<Employee>(
      `${environment.url_api}/employee/${id}`,
      changes
    );
  }
}
