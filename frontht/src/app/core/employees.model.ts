export interface Employee {
  id: number;

  lastname: string;

  second_lastname: string;

  name: string;

  other_name: string;

  country: string;

  type_id: string;

  number_id: string;

  email: string;

  departament: string;

  status: string;

  entry_date: Date;
}
