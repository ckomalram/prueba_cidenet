import { AbstractControl } from '@angular/forms';

export class CustomValidators {
  static isPriceValid(control: AbstractControl) {
    const value = control.value;

    console.log(value);
    if (value > 10000 || value <= 0) {
      return {
        price_invalid: true,
      };
    }
    return null;
  }
}
