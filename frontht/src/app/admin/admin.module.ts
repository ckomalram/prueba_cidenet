import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { EmployeesCreateComponent } from './components/employees-create/employees-create.component';
import { EmployeesListComponent } from './components/employees-list/employees-list.component';
import { EmployeesEditComponent } from './components/employees-edit/employees-edit.component';

@NgModule({
  declarations: [
    NavMenuComponent,
    EmployeesListComponent,
    EmployeesEditComponent,
    EmployeesCreateComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    LayoutModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
})
export class AdminModule {}
