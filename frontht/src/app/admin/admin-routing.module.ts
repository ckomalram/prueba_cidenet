import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeesCreateComponent } from './components/employees-create/employees-create.component';
import { EmployeesEditComponent } from './components/employees-edit/employees-edit.component';
import { EmployeesListComponent } from './components/employees-list/employees-list.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';

const routes: Routes = [
  {
    path: '',
    component: NavMenuComponent,
    children: [
      {
        path: '',
        component: EmployeesListComponent,
      },
      {
        path: 'edit/:id',
        component: EmployeesEditComponent,
      },
      {
        path: 'create',
        component: EmployeesCreateComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
