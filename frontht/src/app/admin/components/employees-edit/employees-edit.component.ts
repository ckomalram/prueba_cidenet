import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { EmployeesService } from 'src/app/core/services/employees.service';
import { CustomValidators } from 'src/app/utils/validators';

@Component({
  selector: 'app-employees-edit',
  templateUrl: './employees-edit.component.html',
  styleUrls: ['./employees-edit.component.css'],
})
export class EmployeesEditComponent implements OnInit {
  form!: FormGroup;
  id!: number;

  constructor(
    private formbuilder: FormBuilder,
    private employeeservice: EmployeesService,
    private router: Router,
    private activateroute: ActivatedRoute
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.activateroute.params.subscribe((params: Params) => {
      // console.log(`Estoy recibiendo el parametro ${params.id}`);
      this.id = params.id;
      this.employeeservice.getEmployee(this.id).subscribe((employee) => {
        this.form.patchValue(employee);
      });
    });
  }

  private buildForm() {
    this.form = this.formbuilder.group({
      lastname: ['', [Validators.required]],
      second_lastname: ['', [Validators.required]],
      name: ['', [Validators.required]],
      other_name: ['', [Validators.required]],
      country: ['', [Validators.required]],
      departament: ['', [Validators.required]],
      type_id: ['', [Validators.required]],
      number_id: ['', [Validators.required]],
      // "lastname": "Concepcion",
      // "second_lastname": "Arauz",
      // "name": "Yoainaris",
      // "other_name": "Yadieth",
      // "country": "Panama",
      // "type_id": "Cedula",
      // "number_id": "8-920-953",
      // "email": "yconcepcion@gmail.com",
      // "departament": "RH",
      // "status": "Active
    });
  }

  saveProduct(event: Event) {
    event.preventDefault();
    console.log(`el formulario esta  ${this.form.valid}`);
    if (this.form.valid) {
      const newEmployee = this.form.value;
      this.employeeservice
        .updateEmployee(this.id, newEmployee)
        .subscribe((employee) => {
          // console.log(employee);
          this.router.navigate(['./admin']);
        });
    }
    // console.log(this.form.value);
  }

  //Custom get para los nombres de los form.
  get lastname() {
    return this.form.get('lastname');
  }
  get second_lastname() {
    return this.form.get('second_lastname');
  }
  get name() {
    return this.form.get('name');
  }
  get other_name() {
    return this.form.get('other_name');
  }
  get country() {
    return this.form.get('country');
  }
  get departament() {
    return this.form.get('departament');
  }
  get number_id() {
    return this.form.get('number_id');
  }
  get type_id() {
    return this.form.get('type_id');
  }
}
