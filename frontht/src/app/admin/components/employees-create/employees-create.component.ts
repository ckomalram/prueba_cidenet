import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Employee } from 'src/app/core/employees.model';
import { EmployeesService } from 'src/app/core/services/employees.service';

@Component({
  selector: 'app-employees-create',
  templateUrl: './employees-create.component.html',
  styleUrls: ['./employees-create.component.css'],
})
export class EmployeesCreateComponent {
  formcreate = this.fb.group({
    lastname: [null, [Validators.required, Validators.maxLength(20)]],
    second_lastname: [null, [Validators.required, Validators.maxLength(20)]],
    name: [null, [Validators.required, Validators.maxLength(20)]],
    other_name: ['', [Validators.maxLength(50)]],
    country: [null, Validators.required],
    type_id: [null, Validators.required],
    number_id: [
      null,
      [Validators.required, Validators.pattern('[a-zA-Z0-9- ]*$')],
    ],
    departament: [null, Validators.required],
  });

  hasUnitNumber = false;

  countries = [
    { name: 'Panamá', abbreviation: 'PTY' },
    { name: 'Colombia', abbreviation: 'COL' },
    { name: 'United States', abbreviation: 'USA' },
  ];

  departaments = [{ name: 'IT' }, { name: 'RH' }, { name: 'Outsourcing' }];

  typesid = [
    { name: 'Cédula Nacional' },
    { name: 'Cédula Extranjera' },
    { name: 'Pasaporte' },
    { name: 'Permiso Especial' },
  ];

  // dominio = [
  //   {colombia: '@cidenet.com.co'},
  //   {panama: 'cidenet.com.pa'},
  //   {usa: 'cidenet.com.us'},
  // ];

  constructor(
    private fb: FormBuilder,
    private employeeservice: EmployeesService,
    private router: Router
  ) {}

  saveProduct(event: Event) {
    event.preventDefault();
    if (this.formcreate.valid) {
      const newEmployee: Employee = this.formcreate.value;
      if (newEmployee.country === 'COL') {
        newEmployee.email = '@cidenet.com.co';
      } else if (newEmployee.country === 'USA') {
        newEmployee.email = '@cidenet.com.us';
      } else {
        newEmployee.email = '@cidenet.com.pa';
      }
      newEmployee.email = newEmployee.name.concat(
        '.',
        newEmployee.lastname,
        newEmployee.email
      );
      newEmployee.status = 'Active';
      console.log(newEmployee);
      this.employeeservice.createEmployee(newEmployee).subscribe((employee) => {
        const tmpEmployee: Partial<Employee> = {
          email: employee.email,
        };
        let extension: string;
        if (employee.country === 'COL') {
          extension = '@cidenet.com.co';
        } else if (employee.country === 'USA') {
          extension = '@cidenet.com.us';
        } else {
          extension = '@cidenet.com.pa';
        }
        employee.email = employee.name.concat(
          '.',
          employee.lastname,
          '.',
          employee.id.toString(),
          extension
        );

        this.employeeservice
          .updateEmployee(employee.id, tmpEmployee)
          .subscribe((resultemail) => {
            console.log(resultemail);
            this.router.navigate(['./admin']);
          });
      });
    }
  }
}
