import { Component, OnInit } from '@angular/core';

import { Employee } from 'src/app/core/employees.model';
import { EmployeesService } from 'src/app/core/services/employees.service';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css'],
})
export class EmployeesListComponent implements OnInit {
  employees: Employee[] = [];
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = [
    'id',
    'name',
    'lastname',
    'identification',
    'country',
    'email',
    'actions',
  ];

  constructor(private employeeservice: EmployeesService) {}

  ngOnInit(): void {
    this.fetchEmployee();
  }

  fetchEmployee() {
    this.employeeservice.getAllEmployee().subscribe((em) => {
      console.log(em);
      this.employees = em;
    });
  }
}
