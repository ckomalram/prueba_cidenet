import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { get } from 'http';
import { CreateEmployeeDto, UpdateEmployeeDto } from '../dtos/employee.dtos';
import { EmployeeService } from '../services/employee.service';

@ApiTags('employee')
@Controller('employee')
export class EmployeeController {
  constructor(private employeeservices: EmployeeService) {}

  @Get()
  @ApiOperation({ summary: 'List of Employees' })
  getEmployees() //   @Query() params: FilterProductsDto
  {
    return this.employeeservices.findAll();
  }

  @Get(':employeeId')
  @ApiOperation({ summary: 'Get one  Employee' })
  getEmployee(@Param('employeeId', ParseIntPipe) employeeId: number) {
    return this.employeeservices.findOne(employeeId);
  }

  @Post()
  @ApiOperation({ summary: 'Create an  Employee' })
  create(@Body() payload: CreateEmployeeDto) {
    return this.employeeservices.create(payload);
  }

  @Put(':employeeId')
  @ApiOperation({ summary: 'Update  an  Employee' })
  update(
    @Param('employeeId', ParseIntPipe) employeeId: number,
    @Body() payload: UpdateEmployeeDto,
  ) {
    return this.employeeservices.update(employeeId, payload);
  }
}
