import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsString,
  IsNumber,
  IsUrl,
  IsDate,
  IsNotEmpty,
  IsPositive,
  IsArray,
  IsOptional,
  Min,
  ValidateIf,
} from 'class-validator';

export class CreateEmployeeDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: ` Lastname of employee` })
  readonly lastname: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: ` second_lastname of employee` })
  second_lastname: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: ` name of employee` })
  name: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ description: ` other_name of employee` })
  other_name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: ` country` })
  country: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: ` Identification of employee ` })
  type_id: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: ` Number of  Identification of employee ` })
  number_id: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ description: ` email of employee , unique` })
  email: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: ` departament of employee` })
  departament: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ description: ` Always Active` })
  status: string;

  @IsDate()
  @IsOptional()
  @ApiProperty({
    description: ` Entry date  of employee, can be after 1 month of today`,
  })
  entry_date: Date;
}
export class UpdateEmployeeDto extends PartialType(CreateEmployeeDto) {}
