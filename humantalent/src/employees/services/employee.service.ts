import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeeDto, UpdateEmployeeDto } from '../dtos/employee.dtos';
import { Employee } from '../entities/employee.entity';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee) private employeeRepo: Repository<Employee>,
  ) {}

  findAll() {
    return this.employeeRepo.find();
  }

  async findOne(id: number) {
    const employee = await this.employeeRepo.findOne(id);
    if (!employee) {
      throw new NotFoundException(`Employee with id ${id} not found!`);
    }

    return employee;
  }

  create(data: CreateEmployeeDto) {
    const newEmployee = this.employeeRepo.create(data);
    // newEmployee.email= newEmployee.name.concat(newEmployee.lastname,newEmployee.id,newEmployee.email);
    // newEmployee.status = 'Active';
    // this.employeeRepo.save(newEmployee);
    console.log(newEmployee);
    return this.employeeRepo.save(newEmployee);
  }

  async update(id: number, changes: UpdateEmployeeDto) {
    const employee = await this.employeeRepo.findOne(id);
    if (!employee) {
      throw new NotFoundException(`Employee with id ${id} not found!`);
    }

    this.employeeRepo.merge(employee, changes);
    return this.employeeRepo.save(employee);
  }
}
