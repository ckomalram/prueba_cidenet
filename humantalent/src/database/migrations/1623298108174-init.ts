import { MigrationInterface, QueryRunner } from 'typeorm';

export class init1623298108174 implements MigrationInterface {
  name = 'init1623298108174';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "employees" ("id" SERIAL NOT NULL, "lastname" character varying(20) NOT NULL, "second_lastname" character varying(20) NOT NULL, "name" character varying(20) NOT NULL, "other_name" character varying(50) NOT NULL, "country" character varying(20) NOT NULL, "type_id" character varying(30) NOT NULL, "number_id" character varying(50) NOT NULL, "email" character varying(300) NOT NULL, "departament" character varying(30) NOT NULL, "status" character varying(10) NOT NULL, "entry_date" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "create_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "update_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), CONSTRAINT "UQ_f4336d746d50dcb2e8de9f2ab2b" UNIQUE ("number_id"), CONSTRAINT "UQ_765bc1ac8967533a04c74a9f6af" UNIQUE ("email"), CONSTRAINT "PK_b9535a98350d5b26e7eb0c26af4" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_460bd72c209c2151f8ca3b1e92" ON "employees" ("email", "number_id") `,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX "IDX_460bd72c209c2151f8ca3b1e92"`);
    await queryRunner.query(`DROP TABLE "employees"`);
  }
}
